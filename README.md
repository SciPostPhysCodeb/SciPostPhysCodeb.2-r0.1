# Codebase release 0.1 for jVMC

by Markus Schmitt, Moritz Reh

SciPost Phys. Codebases 2-r0.1 (2022) - published 2022-08-23

[DOI:10.21468/SciPostPhysCodeb.2-r0.1](https://doi.org/10.21468/SciPostPhysCodeb.2-r0.1)

This repository archives a fixed release as a citable refereed publication. Please refer to the [publication page](https://scipost.org/SciPostPhysCodeb.2-r0.1) to view the full bundle and get information on how to cite it.

For a link to the latest/development version, check the Resources section below.

Copyright Markus Schmitt, Moritz Reh

This README is published under the terms of the CC BY 4.0 license. For the license to the actual codebase, please refer to the license specification in the codebase folder.

## Resources:

* Codebase release version (archive) repository at [https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.2-r0.1](https://git.scipost.org/SciPostPhysCodeb/SciPostPhysCodeb.2-r0.1)
* Live (external) repository at [https://github.com/markusschmitt/vmc_jax/releases/tag/v0.1.7](https://github.com/markusschmitt/vmc_jax/releases/tag/v0.1.7)
